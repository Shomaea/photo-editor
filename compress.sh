#!/bin/bash

# First you should install:
# sudo apt install imagemagick

for image_file in before/*
do
fileName=${image_file#*/}
convert $image_file -resize 600x600 -background white -gravity center -extent 640x640 -quality 70% after/${fileName%.*}.jpg
done